using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetGraph : MonoBehaviour
{
    public bool isLeft = false;
    Sprite currentSprite;
    Image image;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.parent.GetComponent<ImageLoader>().hasChangesR || transform.parent.GetComponent<ImageLoader>().hasChangesL)
            SetImages();

    }

    private void SetImages()
    {
        if (isLeft)
        {
            currentSprite = transform.parent.GetComponent<ImageLoader>().curL;
            transform.parent.GetComponent<ImageLoader>().hasChangesL = false;

        }
        else
        {
            currentSprite = transform.parent.GetComponent<ImageLoader>().curR;
            transform.parent.GetComponent<ImageLoader>().hasChangesR = false;
        }
        image.sprite = currentSprite;
    }
}
