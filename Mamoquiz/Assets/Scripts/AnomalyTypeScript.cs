using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnomalyTypeScript : MonoBehaviour
{
    public GameController gameControllerScript;
    public DetectionHandler detectionHandler;

    public void BenignButton()
    {
        gameControllerScript.diagnoseValue = 1;
        detectionHandler.AddEntry(1);
        gameControllerScript.anomalyPlacementActive = true;
    }

    public void IndeterminedButton()
    {
        gameControllerScript.diagnoseValue = 2;
        detectionHandler.AddEntry(2);
        gameControllerScript.anomalyPlacementActive = true;
    }

    public void MalignantButton()
    {
        gameControllerScript.diagnoseValue = 3;
        detectionHandler.AddEntry(3);
        gameControllerScript.anomalyPlacementActive = true;
    }

}
