using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnomalyScript : MonoBehaviour
{
    private Collider2D col;
    public GameController gameControllerScript;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Something is In");
        if (collision.CompareTag("YellowMarker"))
        {
            Debug.Log("Correctly Marked");
            gameControllerScript.diagnoseValue++;
        }
    }

}
