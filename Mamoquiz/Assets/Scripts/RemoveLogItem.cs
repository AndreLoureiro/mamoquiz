using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveLogItem : MonoBehaviour
{
    public GameObject itemToRemove;

    public void RemoveItem()
    {
        Destroy(itemToRemove);
    }

}
