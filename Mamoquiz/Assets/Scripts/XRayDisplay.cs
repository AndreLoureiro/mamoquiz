using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class XRayDisplay : MonoBehaviour
{
    public XRay xray;

    public TextMeshProUGUI descriptionText;

    public Image leftXRay;
    public Image rightXRay;

    void Start()
    {
        descriptionText.text = xray.description;

        leftXRay.sprite = xray.left;
        rightXRay.sprite = xray.right;
    }

    
}
