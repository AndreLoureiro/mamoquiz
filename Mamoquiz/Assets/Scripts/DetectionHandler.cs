using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class DetectionHandler : MonoBehaviour
{
    GameObject DetectionList;
    GameObject template;

    // Start is called before the first frame update
    void Start()
    {
        template = transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddEntry(int type)
    {
        if (template.transform.parent.childCount < 4)
        {
            DetectionList = Instantiate(template, transform);
            string text = "";
            switch (type)
            {
                case 1:
                    text = "Benign";
                    break;
                case 2:
                    text = "Undetermined";
                    break;
                case 3:
                    text = "Malign";
                    break;
                default:
                    break;

            }

            //DetectionList.transform.GetChild(DetectionList.transform.childCount - 1).GetComponent<TextMeshProUGUI>();
            DetectionList.transform.gameObject.SetActive(true);
            DetectionList.transform.GetChild(DetectionList.transform.childCount - 1).GetComponent<TextMeshProUGUI>().text = text;
        }
    }
}
