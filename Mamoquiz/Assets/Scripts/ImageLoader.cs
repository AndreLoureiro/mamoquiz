using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageLoader : MonoBehaviour
{

    public Sprite[] LGraphies;
    public Sprite[] RGraphies;

    public Sprite curL;
    public Sprite curR;

    public int index = 0;
    public bool hasChangesL = false;
    public bool hasChangesR = false;

    // Start is called before the first frame update
    void Start()
    {
        curL = LGraphies[index];
        curR = RGraphies[index];
        hasChangesL = true;
        hasChangesR = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextItem()
    {
        if(index < LGraphies.Length-1 && index < LGraphies.Length-1)
        {
            index++;
            curL = LGraphies[index];
            curR = RGraphies[index];
            hasChangesL = true;
            hasChangesR = true;
        }
    }

}
