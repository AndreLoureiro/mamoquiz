using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New XRay", menuName = "XRay")]
public class XRay : ScriptableObject
{
    public new string name;
    public new string description;

    public Sprite left;
    public Sprite right;

    public Sprite solution;
}
