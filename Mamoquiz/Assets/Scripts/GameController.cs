using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject anomalyMark;
    public bool anomalyPlacementActive;
    public bool creatingNewDetection;
    public bool eraserToolActive;

    float laserLength = 50f;

    public GameObject CreateNewDetectionPanel;

    public int diagnoseValue;


    public GameObject normalOrAlteredPanel;
    public GameObject detectionCreationPanel;

    
    void Start()
    {
        anomalyPlacementActive = false;
        creatingNewDetection = false;
        eraserToolActive = false;

        diagnoseValue = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);


        if (anomalyPlacementActive)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector3 offset = new Vector3(0, 0, 10);
                RaycastHit2D hit = Physics2D.Raycast(pos, -Vector2.up);

                Debug.DrawRay(pos, -Vector2.up * laserLength, Color.red);
                Debug.Log(hit.collider.tag + " detected");

                if (hit.collider.CompareTag("Drawingboard"))
                {
                    Instantiate(anomalyMark, pos + offset, Quaternion.identity);
                    //Debug.Log("Entering CompareTag");

                    anomalyPlacementActive = false;
                    CreateNewDetection();
                } else
                {
                    Debug.Log("Can't draw there");
                }
                
            }
        }


        if (Input.GetMouseButtonDown(0) && !anomalyPlacementActive)
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(pos, -Vector2.up);
            
            Debug.DrawRay(pos, -Vector2.up * laserLength, Color.red);
            Debug.Log(hit.collider.tag + " detected");

            if (hit.collider.CompareTag("YellowMarker"))
            {
                Destroy(GameObject.FindGameObjectWithTag("YellowMarker"));                
            }
        }        
    }

    //public void ActivateAnomalyPlacement()
    //{
    //    if (anomalyPlacementActive)
    //    {
    //        anomalyPlacementActive = false;
    //        Debug.Log("Pinpoint is Inactive");
    //    } else {
    //        anomalyPlacementActive = true;
    //        Debug.Log("Pinpoint is Active");
    //    }
    //}

    public void CreateNewDetection()
    {
        if (creatingNewDetection)
        {
            creatingNewDetection = false;
            //CreateNewDetectionPanel.SetActive(false);

        } else
        {
            creatingNewDetection = true;
            //CreateNewDetectionPanel.SetActive(true);
        }
    }

    public void ExamHasAnomalies()
    {
        normalOrAlteredPanel.SetActive(false);
        detectionCreationPanel.SetActive(true);
    }

    public void ShowResults()
    {
        Debug.Log(diagnoseValue);
    }
}
